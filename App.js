import React from 'react'
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Card from 'react-bootstrap/Card';
import { Route, Switch,Routes,BrowserRouter } from 'react-router-dom';
import Login from './Components/Login';
import Register from './Register';
import Home from './Components/Home';
import BusTIcket from './Components/BusTIcket';
import Fetch from './Components/Fetch';
import Help from './Components/Help';




function App() {
  return (
    <>
    <div>
      <section>

        
    <div>
      <Navbar expand="lg" className="fixed-top" >
      <Container fluid>
      <Navbar.Brand href='https://www.redbus.in/?gclid=Cj0KCQiAsburBhCIARIsAExmsu57JC1NRFCGS-w6W7sThTNmnM0Ho-ZI9TMzU0qtg5qPMGjczOLIMjYaAt86EALw_wcB'><img src='https://st.redbus.in/Images/rdc/rdc-redbus-logo.svg'/> </Navbar.Brand>
       <Navbar.Brand href="/" style={{'font-family':'cursive'}}>Redbus</Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="me-auto my-2 my-lg-0"
            style={{ maxHeight: '100px' }}
            navbarScroll
          >
            <Nav.Link href="/">Home</Nav.Link>
            <Nav.Link href="/BusTicket">Bus Ticket</Nav.Link>
            <Nav.Link href="/Destinations">Destinations</Nav.Link>
          
          
          </Nav>
          <div style={{marginRight:'20px'}}>
          <Register />
          </div>
          <div style={{marginRight:'20px'}}>
                      <Login /></div>
                      <Nav.Link href="/Help">Help</Nav.Link>
         
       
         
          

          <Form className="d-flex">
            {/* <Form.Control
              type="search"
              placeholder="Search"
              className="me-2"
              aria-label="Search"
            /> */}
            {/* <Button type="submit" className='color-white'>Search</Button> */}
            {/* <button type='submit' color='danger' style={{borderRadius:'15px', backgroundColor:'red'}}>search</button> */}
          </Form>
        </Navbar.Collapse>
      </Container>
      
    </Navbar>
    </div>
   <BrowserRouter>
    <Routes>
    <Route path='/' element={<Home/>} />
    <Route path='/BusTIcket' element={<BusTIcket/>} />
     <Route path='/Destinations' element={<Fetch/>} /> 
     <Route path='/Help' element={<Help/>} /> 
    </Routes>
      {/* <Home/> */}
      {/* <Login /> */}
     
    I
    {/* </div> */}
    </BrowserRouter>
    
    </section>
    </div><br/><br/>
      <div className='footer text-center text-black bg-danger'>
      

    {/* <!-- Section: Form --> */}
    <section class=""><br/><br/>
      <form action="">
        {/* <!--Grid row--> */}
        <div class="row d-flex justify-content-center">
          {/* <!--Grid column--> */}
          <div class="col-auto">
            <p class="pt-2">
              <strong>Sign up for our newsletter</strong>
            </p>
          </div>
          {/* <!--Grid column--> */}

          {/* <!--Grid column--> */}
          <div class="col-md-5 col-12">
            {/* <!-- Email input --> */}
            <div data-mdb-input-init class="form-outline mb-4">
              <input type="email" id="form5Example24" class="form-control" />
              <label class="form-label" for="form5Example24">Email address</label>
            </div>
          </div>
          {/* <!--Grid column--> */}

          {/* <!--Grid column--> */}
          <div class="col-auto">
            {/* <!-- Submit button --> */}
            <button data-mdb-ripple-init type="submit" class="btn btn-outline-white  mb-4" >
              Subscribe
            </button>
          </div>
          {/* <!--Grid column--> */}
        </div>
        {/* <!--Grid row--> */}
      </form>
    </section>
          <p>Search | Terms of services </p>
          <p>Copyright: © 2023 Redbus PVT LTD</p>
          <p>Powerd by Rohitha P</p>
        </div>
    
  
    </>
  )
}

export default App;