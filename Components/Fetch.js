import axios from 'axios';
import React, { useState,useEffect } from 'react';
import Cards from './Cards';
import '../App.js'


function Fetch() {
  const [data,setData] = useState([]);
  useEffect(() =>{
    const fetchData =async () =>{
      try{
        let response = await axios.get("http://localhost:5000/fetch")
        console.log(response.data)
        setData(response.data)  
      }
      catch(err){
      }
    };
    fetchData()
  },[]);

  return (
  <div className='bg-white'>
    <br/>
    <br/>
    <br/>
    <br/>
    <h1 className='text-center text-black'>" REDBUS make Your Journey possible"</h1><br/>
    <h3 className='text-black tect-center mx-200px'>SIMPLE | SAFE | SECURE | JOYFUL</h3>
    <h1 className='text-info text-center text-black fs-10 text-white mt-2'>FIND YOUR CONNECTING ROUTS TO TRAVEL WITH US</h1>
    <div className='container'>
    <div className='row mt-5'>{data.filter((e)=>(e.image,e.cardname,e.cardtitle)).map((e)=>(
      // <img src={e.Img} alt=""  className='w-25  rounded-5'/>
      <Cards data={e}/>
    ))}
  </div>  
  </div>
  </div>      
);
}
export default Fetch;