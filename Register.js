// import React, { useState } from 'react';
// import axios from 'axios'
// import { Button, Modal, ModalHeader, ModalBody, ModalFooter,Form,Input,Label,FormGroup,Row,Col } from 'reactstrap';

// function Register(args) {
//   const [modal, setModal] = useState(false);
// const [formdata, setFormdata] = useState({
//     firstname:'',
//     lastname:'',
//     email:'',
//     password:'',
//     address:'',
//     state:'',
//     city:'',
//     zip:''

// });
// const handleInput = (e) =>{
//     const {name, value} = e.target
//     setFormdata({
//         ...formdata,
//         [name]:value
//     })
// }
// console.log(formdata)
//   // const toggle = () => setModal(!modal);
//   const toggle = (e) =>{
//     e.preventDefault();
//     setModal(!modal);
//   }
//   const handleSubmit = async (e) =>{
//     e.preventDefault();
//     try{
//       let response = await axios.post("http://localhost:5000/register",formdata)
//       console.log(response)
//       alert("data inserted");
//     }catch (err){
//       throw err
//     }
//    }
//   return (
//     <div>
//       <Button color="danger" onClick={toggle}>
//         Register Me
//       </Button>
//       <Modal isOpen={modal} toggle={toggle} {...args}>
//         <ModalHeader toggle={toggle}>Registration Form</ModalHeader>
//         <ModalBody>

// <Form onSubmit={handleSubmit}>
//   <Row>
//     <Col md={6}>
//       <FormGroup>
//         <Label for="exampleFirstname">
//         Firstname
//         </Label>
//         <Input
//           id="exampleFirstname"
//           name="firstname"
//           placeholder="firstname"
//           type="Firstname"
//           value={formdata.firstname}
//           onChange={handleInput}
//           required
//         />
//       </FormGroup>
//     </Col>
//     <Col md={6}>
//       <FormGroup>
//         <Label for="exampleLastname">
//         Lastname
//         </Label>
//         <Input
//           id="exampleLastname"
//           name="lastname"
//           placeholder="lastname"
//           type="lastname"
//           value={formdata.lastname}
//           onChange={handleInput}
//           required
//         />
//       </FormGroup>
//     </Col>
//   </Row>
//   <Row>
//     <Col md={6}>
//       <FormGroup>
//         <Label for="exampleEmail">
//           Email
//         </Label>
//         <Input
//           id="exampleEmail"
//           name="email"
//           placeholder="with a placeholder"
//           type="email"
//           value={formdata.email}
//           onChange={handleInput}
//           required
//         />
//       </FormGroup>
//     </Col>
//     <Col md={6}>
//       <FormGroup>
//         <Label for="examplePassword">
//           Password
//         </Label>
//         <Input
//           id="examplePassword"
//           name="password"
//           placeholder="password placeholder"
//           type="password"
//           value={formdata.password}
//           onChange={handleInput}
//           required
//         />
//       </FormGroup>
//     </Col>
//     </Row>
//     <Row>
//     <FormGroup>
//     <Label for="exampleAddress">
//       Address
//     </Label>
//     <Input
//       id="exampleAddress"
//       name="address"
//       placeholder="6-51/A,..."
//       value={formdata.address}
//       onChange={handleInput}
//       required
//     />
//   </FormGroup>
//   <FormGroup>
//     <Label for="exampleCity">
//       City
//     </Label>
//     <Input
//       id="exampleCity"
//       name="city"
//       placeholder="Apartment,etc"
//       value={formdata.city}
//       onChange={handleInput}
//       required
//     />
//   </FormGroup>
//   </Row>
//   <Row>
//   <Col md={6}>
//       <FormGroup>
//         <Label for="exampleState">
//           State
//         </Label>
//         <Input
//           id="exampleState"
//           name="state"
//           value={formdata.state}
//           onChange={handleInput}
//           required
//         />
//       </FormGroup>
//     </Col>
//     <Col md={6}>
//       <FormGroup>
//         <Label for="exampleZip">
//           Zip
//         </Label>
//         <Input
//           id="exampleZip"
//           name="zip"
//           value={formdata.zip}
//           onChange={handleInput}required
//         />
//       </FormGroup>
//     </Col>
//   </Row>
//   <FormGroup check>
//     <Input
//       id="exampleCheck"
//       name="check"
//       type="checkbox"
//     />
//     <Label
//       check
//       for="exampleCheck"
//     >
//       Check me out
//     </Label>
//   </FormGroup>
//     <Button type='submit'>
//       registration
//     </Button>
//   </Form>
//         </ModalBody>
//         <ModalFooter>
//           <Button color="primary" onClick={toggle}>
//             Do Something
//           </Button>{' '}
//           <Button color="secondary" onClick={toggle}>
//             Cancel
//           </Button>
//         </ModalFooter>
//       </Modal>
//     </div>
//   );
// }

// export default Register;

import React, { useState } from 'react';
import axios from 'axios'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter,Form,Input,Label,FormGroup,Row,Col } from 'reactstrap';

function Register(args) {
  const [modal, setModal] = useState(false);
const [formdata, setFormdata] = useState({
    Username:'',
    // BloodGroup:'',
    Email:'',
    Password:'',
    // Availability:'',
    Country:'',
    City:'',
    District:'',
    State:'',
    Pincode:''

});
const handleInput = (e) =>{
    const {name, value} = e.target
    setFormdata({
        ...formdata,
        [name]:value
    })
}
console.log(formdata)
  const toggle = () => setModal(!modal);
  /*const toggle = () =>{
    e.preventDefault();
    setModal(!modal);
  }*/
  const handleSubmit = async (e) =>{
    e.preventDefault();
    try{
      let response =  await axios.post('http://localhost:5000/register',formdata)
      console.log(response)
      alert("Successfully Registerd")
    }catch (err){
      throw err
    }
   }
  return (
    <div>
      <Button color="danger" onClick={toggle}>
        REGISTER
      </Button>
      <Modal isOpen={modal} toggle={toggle} {...args}>
        <ModalHeader toggle={toggle}>Registration Form</ModalHeader>
        <ModalBody>

<Form onSubmit={handleSubmit}>
  <Row>
    <Col md={6}>
      <FormGroup>
        <Label for="exampleUsername">
        Username
        </Label>
        <Input
          id="exampleUsername"
          name="username"
          placeholder="username"
          type="username"
          value={formdata.username}
          onChange={handleInput}
          required
        />
      </FormGroup>
    </Col>
    {/* <Col md={6}>
      <FormGroup>
        <Label for="example">
       BloodGroup
        </Label>
        <Input
          id="exampleBloodGroup"
          name="Bloodgroup"
          placeholder="Bloodgroup"
          type="Bloodgroup"
          value={formdata.Bloodgroup}
          onChange={handleInput}
          required
        />
      </FormGroup>
    </Col> */}
  </Row>
  <Row>
    <Col md={6}>
      <FormGroup>
        <Label for="exampleEmail">
          Email
        </Label>
        <Input
          id="exampleEmail"
          name="email"
          placeholder="with a placeholder"
          type="email"
          value={formdata.email}
          onChange={handleInput}
          required
        />
      </FormGroup>
    </Col>
    <Col md={6}>
      <FormGroup>
        <Label for="examplePassword">
          Password
        </Label>
        <Input
          id="examplePassword"
          name="password"
          placeholder="password placeholder"
          type="password"
          value={formdata.password}
          onChange={handleInput}
          required
        />
      </FormGroup>
    </Col>
    </Row>
    {/* <Col md={6}>
      <FormGroup>
        <Label for="example">
         	
        </Label>
        <Input
          id="exampleAvailability"
          name="Availability	"
          placeholder=" "
          type="text"
          value={formdata.text}
          onChange={handleInput}
          required
        />
      </FormGroup>
    </Col> */}
    
   
    <Row>
    <FormGroup>
    <Label for="exampleCountry">
      Country
    </Label>
    <Input
      id="exampleCountry"
      name="country"
      placeholder="India"
      value={formdata.country}
      onChange={handleInput}
      required
    />
  </FormGroup>
  <FormGroup>
    <Label for="exampleCity">
      City
    </Label>
    <Input
      id="exampleCity"
      name="city"
      placeholder="ex:knr"
      value={formdata.city}
      onChange={handleInput}
      required
    />
  </FormGroup>
  <FormGroup>
    <Label for="exampleDistrict">
      District
    </Label>
    <Input
      id="exampleDistrict"
      name="district"
      placeholder=""
      value={formdata.district}
      onChange={handleInput}
      required
    />
  </FormGroup>
  </Row>
  <Row>
  <Col md={6}>
      <FormGroup>
        <Label for="exampleState">
          State
        </Label>
        <Input
          id="exampleState"
          name="state"
          value={formdata.state}
          onChange={handleInput}
          required
        />
      </FormGroup>
    </Col>
    <Col md={6}>
      <FormGroup>
        <Label for="examplePincode">
         Pincode
        </Label>
        <Input
          id="examplePincode"
          name="pincode"
          value={formdata.pincode}
          onChange={handleInput}required
        />
      </FormGroup>
    </Col>
  </Row>
  <FormGroup check>
    <Input
      id="exampleCheck"
      name="check"
      type="checkbox"
    />
    <Label
      check
      for="exampleCheck"
    >
      Check me out
    </Label>
  </FormGroup><br/>
    <Button type='Sign in'>
      Register
    </Button>
  </Form>
        </ModalBody>
        <ModalFooter>
          {/* <Button color="primary" onClick={toggle}>
            Do Something
          </Button>{' '} */}
          <Button color="secondary" onClick={toggle}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
}

export default Register;